const actions = {
  setView ({commit}, view) {
    commit("setView", view)
  }
}
export default actions