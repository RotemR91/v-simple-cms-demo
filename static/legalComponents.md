## Legal Vuetify Component names

| Name | Description |
| ---- | ----------- |
| <span style="color: red">v-text-field</span> | no extra configuration required |
| <span style="color: red">v-textarea | no extra configuration required |
| <span style="color: red">v-switch | no extra configuration required but assumes that the value is a boolean! |
| <span style="color: red">v-checkbox | no extra configuration required but assumes that the value is a boolean! |
| <span style="color: red">v-slider | must provide a min and max props |
| <span style="color: red">v-range-slider | must provide a min and max props and value must be a string-array ("[0, 100]") |
| <span style="color: red">v-select | must provide a list of selections ([1,2,3,4]) |
| <span style="color: red">v-overflow-btn | must provide a list of selections ([1,2,3,4]) |
| <span style="color: red">v-rating | value must be a number |