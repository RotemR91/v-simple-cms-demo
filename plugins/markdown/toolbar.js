function action (editor, {prefix, suffix, content}) {
    editor.insert((selected) => {
        return ({
            text: `${prefix}${content(selected)}${suffix}`,
            selected: content(selected)
        })
    })
}
function align (direction) {
    return ({
        prefix: `\n::: align-${direction}\n`,
        suffix: ':::\n',
        content: (selected) => `${(selected || direction)}\n`
    })
}
function codeBlock ({language='', data='Insert Code'}) {
    return ({
        prefix: '\n```'+language,
        suffix: '```\n',
        content: (selected) => selected || data
    })
}
//
export const highlight = {
    title: 'highlight (Ctrl+Shift+h)',
    icon: 'mdi mdi-marker',
    modifier: 'ctrlShift',
    key: 'h',
    action: (editor) => action(editor, {
        prefix: '==',
        suffix: '==',
        content: (selected) =>  selected || 'placeholder'
    })
}

export const line_break = {
    title: 'line break (Ctrl+Shift+minus(-))',
    icon: 'mdi mdi-keyboard-space',
    modifier: 'ctrlShift',
    key: '-',
    action: (editor) => action(editor, {
        prefix: '',
        suffix: '</br>',
        content: (selected) =>  selected || ''
    })
}

export const check = {
    title: 'checkbox (Ctrl+Shift+o)',
    icon: 'mdi mdi-check-all',
    modifier: 'ctrlShift',
    key: 'o',
    action: (editor) => action(editor, {
        prefix: '\n- [x] ',
        suffix: '\n',
        content: (selected) =>  selected || 'Item'
    })
}

export const math = {
    title: 'math',
    icon: 'mdi mdi-math-integral',
    menus: [
        {
            name: 'normal',
            text: 'normal',
            action: (editor) => action(editor, {
                prefix: '$',
                suffix: '$',
                content: (selected) =>  selected || 'x+1=y'
            })
        },
        {
            name: 'centered',
            text: 'centered',
            action: (editor) => action(editor, {
                prefix: '\n$$',
                suffix: '$$\n',
                content: (selected) =>  selected || 'x+1=y'
            })
        }
    ]
}

export const code = {
    title: 'code',
    icon: 'mdi mdi-xml',
    menus: [
        {
            name: 'python',
            text: 'python',
            action: (editor) => action(editor, codeBlock({language: 'python'}))
        },
        {
            name: 'javascript',
            text: 'javascript',
            action: (editor) => action(editor, codeBlock({language: 'js'}))
        }
    ]
}

export const underline = {
    title: 'under line (Ctrl+Shift+u)',
    icon: 'mdi mdi-format-underline',
    modifier: 'ctrlShift',
    key: 'u',
    action: (editor) => action(editor, {
        prefix: '++',
        suffix: '++',
        content: (selected) =>  selected || 'placeholder'
    })
}

export const image = {
    title: 'image (Ctrl+Shift+i)',
    icon: 'mdi mdi-image',
    modifier: 'ctrlShift',
    key: 'i',
    action: (editor) => action(editor, {
        prefix: '![',
        suffix: '](https://)',
        content: (selected) =>  selected || 'Description'
    })
}

export const alignment = {
    title: 'alignment',
    icon: 'mdi mdi-align-horizontal-left',
    menus: [
        {
            name: 'center',
            text: 'center',
            modifier: 'ctrlShift',
            key: 'c',
            action: (editor) => action(editor, align('center'))
        },
        {
            name: 'left',
            text: 'left',
            modifier: 'ctrlShift',
            key: 'l',
            action: (editor) => action(editor, align('left'))
        },
        {
            name: 'right',
            text: 'right',
            modifier: 'ctrlShift',
            key: 'r',
            action: (editor) => action(editor, align('right'))
        }
    ]
}

export const charts = {
    title: 'charts',
    icon: 'mdi mdi-chart-pie',
    menus: [
        {
            name: 'flow chart',
            text: 'flow chart',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\nflowchart LR
                \n\tA-->B\n`
            }))
        },
        {
            name: 'subgraphs',
            text: 'subgraphs',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\nflowchart TB
                \n\tc1-->a2
                \n\tsubgraph one
                \n\ta1-->a2
                \n\tend\n`
            }))
        },
        {
            name: 'sequence',
            text: 'sequence',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\nsequenceDiagram
                \n\tautonumber
                \n\tAlice->>John: Hello John, how are you?
                \n\tloop Healthcheck
                \n\t\tJohn->>John: Fight against hypochondria
                \n\tend\n`
            }))
        },
        {
            name: 'class',
            text: 'class',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\nclassDiagram
                \n\tdirection RL
                \n\tclass Student {
                \n\t\t-idCard : IdCard
                \n\t}
                \n\tclass IdCard{
                \n\t\t-id : int
                \n\t\t-name : string
                \n\t}
                \n\tStudent "1" --o "1" IdCard : carries\n`
            })) 
        },
        {
            name: 'state',
            text: 'state',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\nstateDiagram
                \n\tdirection LR
                \n\t[*] --> A
                \n\tA --> B
                \n\tB --> C
                \n\tstate B {
                \n\t\tdirection LR
                \n\t\ta --> b
                \n\t}
                \n\tB --> D\n`
            }))
        },
        {
            name: 'pie',
            text: 'pie',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\npie title Pie Chart
                \n\t"A" : 386
                \n\t"B" : 85
                \n\t"C" : 15\n`
            }))
        },
        {
            name: 'gantt',
            text: 'gantt',
            action: (editor) => action(editor, codeBlock({
                language: 'mermaid',
                data: `\ngantt
                \n\ttitle A Gantt Diagram
                \n\tdateFormat  YYYY-MM-DD
                \n\tsection Section
                \n\tA task           :a1, 2014-01-01, 30d
                \n\tAnother task     :after a1  , 20d
                \n\tsection Another
                \n\tTask in sec      :2014-01-12  , 12d
                \n\tanother task      : 24d\`.\n`
            }))
        }
    ]
}