::: details Show Code
```vue
<template>
  <v-simple-cms
    ref="cms"
    :data-objects="dataObjects"
    :configs="configs"
    :card-class="'pa-3'"
    :search-keys="searchKeys"
    :disable-key-addition="true"
    @backup="backup"
  >
    <template #@image="{value}">
      <div v-if="value.image !== undefined">
        <v-text-field
          v-model="value.image"
          prepend-icon="mdi-tooltip-image"
        />
        <PreviewImage text="Preview image" :image="value.image" />
      </div>
    </template>
    <template #@text="{value}">
      <v-md-editor
        v-if="value.text !== undefined" 
        v-model="value.text"
      />
    </template>
    <template #actions="{value, key, id}">
      <v-row class="ml-0">
        <v-col cols="12">
          <v-btn @click="save(value, key, id)" color="success">Save</v-btn>
          <v-btn :disabled="id === 'template'" @click="remove(key, id)" color="error">Remove</v-btn>
          <v-btn @click="preview(key, id)" color="primary">Preview JSON</v-btn>
          <v-btn @click="download(key, id)" color="accent">Download JSON</v-btn>
        </v-col>
      </v-row>
    </template>
  </v-simple-cms>
</template>

<script>
import PreviewImage from "@/components/Demo/PreviewImage"
import axios from "axios"

export default {
  name: "Advance",
  components: {
    PreviewImage, 
  },
  async created () {
    let key
    for (const file of this.files) {
      key = file.split('.')[0]
      this.keys.push(key)
      await this.getAndSetData(`templates/${file}`, key, "template")
      await this.getAndSetData(`data/${file}`, key, "")
    }
  },
  data () {
    return {
      tab: null,
      dataObjects: {},
      configs: {},
      searchKeys: {},
      keys: [],
      unsubscribe: {},
      files: [
        "blogPosts.json",
        "members.json",
        "publications.json",
        "test.json"
      ]
    }
  },
  methods: {
    async getAndSetData (path, key, type) {
      let data = await axios.get(path)
      data = data.data
      if (type === "template") {
        this.extendDataObjects(key, {template: data.template})
        this.$set(this.configs, key, data.config)
        this.$set(this.searchKeys, key, data.searchKey)
      } else {
        this.extendDataObjects(key, data)
      }
    },
    extendDataObjects (key, value) {
      if (this.dataObjects[key] == null) {
        this.$set(this.dataObjects, key,  value)
      } else {
        this.$set(this.dataObjects, key, {...this.dataObjects[key], ...value})
      }
    },
    getIndex (arr) {
      return Math.max(...arr.map(item => item.index)) + 10
    },
    save (value, key, id) {
      const data = value
      if (id === 'template') {
        data.index = this.getIndex(Object.keys(value).length)
      }
      console.log(data, id, key)
    },
    remove (key, id) {
      console.log(key, id)
    },
    preview (key, id) {
      this.$refs.cms.editorAccess(key, id).previewJSON()
    },
    download (key, id) {
      this.$refs.cms.editorAccess(key, id).exportJSON()
    },
    backup ({data, name}) {
      data = JSON.stringify(data, null, '\t')
      this.$refs.cms.editorAccess(Object.keys(this.dataObjects)[0], 'template').exportFile(data, name, "application/json", "json")
    }
  }
}
</script>
```
:::
**Choose a template from the left-side menu**