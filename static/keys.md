## Objects/Arrays representations and keys

### Syntax

1. **Object** representation - An object is represented as a string where each key-level is separated by the `separator` prop. 

    Example:
    ```js
    a: {
      b: {
        c: {
        }
      }
    }
    ```
    Will be represented by: `a.b.c`

2. **Array** representation - An array is represented as a string where each key-level is separated by the `separator` prop and the last character must be a number representing the index to be added (Remark: indices must be entered in order, otherwise the new key will not be added).

    Example:
    ```js
    a: {
      b: {
        c: [

        ]
      }
    }
    ```
    Will be represeneted by: `a.b.c.0`

3. **String** representation: A string is represented by a string with **no** `separator`.

### Editing

When extending an object or array which already existed in the template (file the was loaded by the user) the following will occur:

1. Extending an array of strings: A new index is added to the array.
2. Extending an array of objects: A new index with a template of the object will be added to the array.

    Example:

    Array before extension
    ```js
    a: [
      {
        x: "1",
        y: "1"
      }
    ]
    ```
    Array after extension
    ```js
    a: [
      {
        x: "1",
        y: "1"
      },
      {
        x: "",
        y: ""
      }
    ]
    ```

    **In case that the original array has a structure where the objects doesn't have the same structure a pop-up for adding a new key to any of the sub-keys in the array-objects will appear.

3. Extending an object: a pop-up for adding a new key to any of the sub-keys in the array-objects will appear.


### Rules

| name | descriptipn |
| ---- | ----------- |
| characters | no special characters apart for `separator` and `_` are allowed |
| separatorStart | no `separator` at begining of a key |
| separatorEnd | no `separator` at end of a key |
| consecutiveSeparators | no 2 (or more) consecutive `separator` allowed |
| keyExist | No overwriting of existing keys |
| consecutiveArrayIndex | array index must be consecutive |
| noOverwritingOfKV | no overwriting of an existing key-value pair |
| noOverwritingOfType | no overwriting of type | 