export const createMarkdownPlugin = ({plugin,options}) => {
  const extendMarkdown = function (mdParser) {
    if (mdParser) {
      mdParser.use(plugin, {
        ...options
      });
    }
  };

  return {
    install(VMdEditor) {
      VMdEditor.extendMarkdown(extendMarkdown);
    },
  };
};