import Vue from 'vue';

import VueMarkdownEditor from '@kangc/v-md-editor/lib/codemirror-editor';
import '@kangc/v-md-editor/lib/style/codemirror-editor.css';

// Resources for the codemirror editor
import Codemirror from 'codemirror';
// mode
import 'codemirror/mode/markdown/markdown';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/css/css';
import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/mode/vue/vue';
// edit
import 'codemirror/addon/edit/closebrackets';
import 'codemirror/addon/edit/closetag';
import 'codemirror/addon/edit/matchbrackets';
// placeholder
import 'codemirror/addon/display/placeholder';
// active-line
import 'codemirror/addon/selection/active-line';
// scrollbar
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/addon/scroll/simplescrollbars.css';
// style
import 'codemirror/lib/codemirror.css';

import Prism from 'prismjs';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';

VueMarkdownEditor.Codemirror = Codemirror;
VueMarkdownEditor.use(vuepressTheme, {
    Prism,
    codeHighlightExtensionMap: {
        vue: 'html',
    }
})

//enable linkify
VueMarkdownEditor.extendMarkdown(mdParser => {
    mdParser.options.linkify = true
})

// extend toolbars - the leftToolbar variable under store/state.js has to be extended to accomodate the extension
import * as extensions from "@/plugins/markdown/toolbar"

function attachHotkey (title, action, modifier, key) {
    VueMarkdownEditor.command(title, action)
    VueMarkdownEditor.hotkey({
        modifier,
        key,
        action(editor) {
            editor.execCommand(title);
        }
    })
}

const toolbarItems = Object.values(extensions)
toolbarItems.forEach(item => {
    if (item?.key && item?.modifier) {
        attachHotkey(item.title, item.action, item.modifier, item.key)
    }
    if (item?.menus) {
        item.menus.forEach(subItem => {
            if (subItem?.key && subItem.modifier) {
                attachHotkey(subItem.name, subItem.action, subItem.modifier, subItem.key)
            }
        })
    }
    VueMarkdownEditor.toolbar(item.title, item)
})

// katex - latex math formulas
// import createKatexPlugin from '@kangc/v-md-editor/lib/plugins/katex/cdn';
// import '@kangc/v-md-editor/lib/theme/style/github.css';
// VueMarkdownEditor.use(createKatexPlugin())

// mermaid charts
// import createMermaidPlugin from '@kangc/v-md-editor/lib/plugins/mermaid/cdn';
// import '@kangc/v-md-editor/lib/plugins/mermaid/mermaid.css';
// VueMarkdownEditor.use(createMermaidPlugin())

// checkboxes / todo lists
import createTodoListPlugin from '@kangc/v-md-editor/lib/plugins/todo-list/index';
import '@kangc/v-md-editor/lib/plugins/todo-list/todo-list.css';
VueMarkdownEditor.use(createTodoListPlugin())

// code snippets line numbering 
import createLineNumbertPlugin from '@kangc/v-md-editor/lib/plugins/line-number/index';
VueMarkdownEditor.use(createLineNumbertPlugin())

import createHighlightLinesPlugin from '@kangc/v-md-editor/lib/plugins/highlight-lines/index';
import '@kangc/v-md-editor/lib/plugins/highlight-lines/highlight-lines.css';
VueMarkdownEditor.use(createHighlightLinesPlugin())

// copy code snippets
import createCopyCodePlugin from '@kangc/v-md-editor/lib/plugins/copy-code/index';
import '@kangc/v-md-editor/lib/plugins/copy-code/copy-code.css';
VueMarkdownEditor.use(createCopyCodePlugin());

// align (left, center, right)
import createAlignPlugin from '@kangc/v-md-editor/lib/plugins/align';
VueMarkdownEditor.use(createAlignPlugin());

// custom messages boxes
import createTipPlugin from '@kangc/v-md-editor/lib/plugins/tip/index';
import '@kangc/v-md-editor/lib/plugins/tip/tip.css';
VueMarkdownEditor.use(createTipPlugin());

// emojies
import createEmojiPlugin from '@kangc/v-md-editor/lib/plugins/emoji/index';
import '@kangc/v-md-editor/lib/plugins/emoji/emoji.css';
VueMarkdownEditor.use(createEmojiPlugin());

// language
import enUS from '@kangc/v-md-editor/lib/lang/en-US';
VueMarkdownEditor.lang.use('en-US', enUS);

// extensions
import { createMarkdownPlugin } from '@/plugins/markdown/md-plugin'

// footnote - buggy with vue
// import { footnote_plugin } from '@/plugins/markdown/markdown-it-footnote'
// VueMarkdownEditor.use(createMarkdownPlugin({plugin: footnote_plugin}));

// subscript
import { sub_plugin } from '@/plugins/markdown/markdown-it-subscript'
VueMarkdownEditor.use(createMarkdownPlugin({plugin: sub_plugin}));
// superscript
import { sup_plugin } from '@/plugins/markdown/markdown-it-superscript'
VueMarkdownEditor.use(createMarkdownPlugin({plugin: sup_plugin}));
// insert
import { ins_plugin } from '@/plugins/markdown/markdown-it-insert'
VueMarkdownEditor.use(createMarkdownPlugin({plugin: ins_plugin}));
// mark
import { mark_plugin } from '@/plugins/markdown/markdown-it-mark'
VueMarkdownEditor.use(createMarkdownPlugin({plugin: mark_plugin}));
// anchor - automatic ids to headers
import anchor from '@/plugins/markdown/anchor/markdown-it-anchor'
VueMarkdownEditor.use(createMarkdownPlugin({plugin: anchor}));
// video - youtube and viemo (could be extended with proper regex)
import { video_plugin } from '@/plugins/markdown/markdown-it-video'
VueMarkdownEditor.use(createMarkdownPlugin({plugin: video_plugin}));


// HTML whitelist
VueMarkdownEditor.xss.extend({
    whiteList: {
      iframe: ['src', 'class', 'type', 'width', 'height', 'frameborder', 'webkitallowfullscreen', 'mozallowfullscreen', 'allowfullscreen'],
    }
});

Vue.use(VueMarkdownEditor);