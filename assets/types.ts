export interface CustomObject<T> {
  [key: string]: T
}

export interface TemplateTypes {
  value: string,
  text: string
}

export interface RulesFunction {
  (value: string): string|boolean
}
export type KeyRules = CustomObject<RulesFunction>

export type sortedAndGroupedTemplateData = CustomObject<Array<CustomObject<string>>>

export interface FieldTypeFunction {
  (str: string): string
}

export interface FileObject {
  result: CustomObject<string> | string,
  type: string
}