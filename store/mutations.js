import Vue from "vue"

const mutations = {
  setView (state, view) {
    state.view = view
  }
}
export default mutations