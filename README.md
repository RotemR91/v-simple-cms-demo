# v-simple-cms-demo

### Install

```
npm i v-simple-cms
```

### Documentation

[https://rotemr91.gitlab.io/v-simple-cms-demo/](https://rotemr91.gitlab.io/v-simple-cms-demo/)

### Package repository

[https://gitlab.com/RotemR91/v-simple-cms-package](https://gitlab.com/RotemR91/v-simple-cms-package)

