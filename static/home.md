## Introduction

`v-simple-cms` is a library that allows the easy manipulation and organiztion of JSON like objects. With the help of simple configuration templates it allows creating complicated forms to manage and export data. In its core `v-simple-cms` uses `v-editor` which allows to load, edit, extend, convert and export yaml and json files in the browser.
Read the [documentation](/api).

### Installation

```sh
npm i v-simple-cms
```


