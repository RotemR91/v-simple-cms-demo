# Components

## `Editor`

The `Editor` component is the underlying logic of the entire library. All of its props are accessible through `VEditor` and `VSimpleCms`.

### Props

| Name | type | Default | Description |
| -----| ---- | ------- | ----------- |
| **dataObj**| <span class="green--text">Object</span> | <span class="primary--text">null</span> | The data object to be passed. Can be any JSON-like format object. |
| **config** | <span class="green--text">Object</span> | <span class="primary--text">{}</span> | The corrosponding config to the the dataObj prop. Check [here](/#configuration) for the necessary structure.|
| **baseURL** | <span class="green--text">String</span> | <span class="primary--text">""</span> | the base url to user templates |
| **templateTypes** | <span class="green--text">TemplateTypes[]</span> | <span class="primary--text">[]</span> | Array of values and texts representing the user defined templates. Check [here](/#types.TemplateTypes) for the type definition. |
| **keyRules** | <span class="green--text">KeyRules</span> | <span class="primary--text">null</span> | An Object of string:function pairs where function returns a Boolean or an error string. Check Check [here](/#types.KeyRules) for the type definition. |
| **textRules** | <span class="green--text">KeyRules</span> | <span class="primary--text">null</span> | An Object of string:function pairs where function returns a Boolean or an error string. Check Check [here](/#types.KeyRules) for the type definition. |
| **overwriteRules** | <span class="green--text">Boolean</span> | <span class="primary--text">false</span> | Ignore all localky defined rules. |
| **separator** | <span class="green--text">String</span> | <span class="primary--text">"."</span> | A string separating sub-keys of Objects/Arrays. Ex: "a.b.0" is {"a": {"b": "c": ["New"]}}
| **downScrollPos** | <span class="green--text">Number</span> | <span class="primary--text">50</span> | Position to scroll down to |
| **disableGoToButtons** | <span class="green--text">Boolean</span> | <span class="primary--text">false</span> | Disable scroll buttons of the `GoToFab` component |
| **disableDeletion** | <span class="green--text">Boolean</span> | <span class="primary--text">false</span> | Items can not be deleted by the user |
| **flat** | <span class="green--text">Boolean</span> | <span class="primary--text">true</span> | Flatten the component's `v-card` |
| **sortByIndex** | <span class="green--text">Boolean</span> | <span class="primary--text">false</span> | Sort items by provided index (in the config). Default sort is by key. |
| **enableDataInput** | <span class="green--text">Boolean</span> | <span class="primary--text">true</span> | Enable file input of JSON/YAML files and their corrosponding configuartion. |
| **groupsNumber** | <span class="green--text">Number</span> | <span class="primary--text">10</span> | Number of groups to show per page in the top data-iterator. **Remark:** For large objects with many items the maximum recommanded value is 5 | 
| **childrenNumber** | <span class="green--text">Number</span> | <span class="primary--text">10</span> | Number of children to show per page in the inner data-iterator. **Remark:** For large objects with many items the maximum recommanded value is 30 | 

### Events

| Name | Description |
| ---- | ----------- |
| **delete** | Emitted when a key is set for deletion. Passes the key as a <span class="green--text">string</span>. |

### Slots

| Name | Description |
| ---- | ----------- |
| **config-creation** | Replaces the Create Config/Cancle config creation buttons. Binds: `config`. |
| **after-key-field** | Located after the `NewKeyField` component and before `ExpansionFields`. |
| **customComponent-[key]** OR in short **@[key]** | The user can pass custom components that can interact with the cms data. It's useful when the default legal components are no enough. Binds: `dataModel` (`dataObj` / file that was loaded by the user) and `rules` (keyRules).
| **expansion-group-[key]** | One row above each group. **key** must be a legal parent level object-key which was passed in `dataObj` |
| **expansion-field-[key]** | One row above each item in the group. **key** must be a legal child level object-key which was passed in `dataObj` |
| **under-expansion-field-[key]** | One row underneath each item in the group. **key** must be a legal child level object-key which was passed in `dataObj`|
| **actions** | Replaces the actions buttons at the bottom of the editor |

### References

| Name | Descrition |
| ---- | ---------- |
| **newKeyField** | Reference to the `NewKeyField` component |
| **expansionFields** | Reference to the `ExpansionFields` component | 

### Useful Functions

```ts
exportFile (data:any, name:string = "backup", type:string , ext:string): void {
  if (type == null) { return }
  const blob = new Blob([data], { type })
  const url = URL.createObjectURL(blob)
  const hiddenElement = document.createElement("a")
  hiddenElement.href = url
  hiddenElement.target = "_blank"
  hiddenElement.download = `${name}.${ext}`
  hiddenElement.click()
}
```

Creates a download for a given data (of any type). MIME type and extention are necessary.

```ts
exportJSON (): void {
  // @ts-ignore
  this.exportFile(JSON.stringify(this.exportData(), null, '\t'), "backup", "application/json", "json")
},
previewJSON (): void {
  this.dialog = true
  // @ts-ignore
  this.preview = JSON.stringify(this.exportData(), null, '\t')
},
exportYAML (): void {
  // @ts-ignore
  this.exportFile(yaml.dump(this.exportData()), "backup", "application/x-yaml", "yaml")
},
previewYAML (): void {
  this.dialog = true
  // @ts-ignore
  this.preview = yaml.dump(this.exportData())
}
```

Export and preview functions


## `VEditor`

The `VEditor` component is a wrapper to `Editor`. It is necessary in order to allow the usage of `v-file-input` as a legal input component. When an item is configured to be a `v-file-input` the user can choose to extend the current work place with an additional file or pass a string representing the contents of the file.
All of `Editor` props can be passed directly to `VEditor` and they will passed to `Editor` seemingly. It **does not** define any extra props/slots/events.

### References

| Name | Descrition |
| ---- | ---------- |
| **editor** | Reference to the `Editor` component |

## `VSimpleCms`

The `VSimpleCms` component is a wrapper to `VEditor`. It allows passing an Object of Objects (that would normally be passed to VEditor) and creates a panel using `VTabs` where each tab is a `VEditor` instance.

### Props

| Name | type | Default | Description |
| ---- | ---- | ------- | ----------- |
| **dataObjects** | <span class="green--text">Object</span> | <span class="primary--text">{}</span> | an Object of Objects (that would normally be passed to VEditor) and creates a panel using `VTabs` where each tab is a `VEditor` instance. |
| **configs** | <span class="green--text">Object</span> | <span class="primary--text">{}</span> | an Object of Objects of configurations. One for each sub-object in **dataObjects**. |
| **isSearch** | <span class="green--text">Boolean</span> | <span class="primary--text">true</span> | Enable search of sub-objects according to a configuration. Check [here](/#configuration) for more details. |
| **searchKeys** | <span class="green--text">Object</span> | <span class="primary--text">{}</span> | An Object of group-search keys pairs as set in all the configurations |

### Attributes

| Name | Description |
| ---- | ----------- |
| **card-[key]** | All [`v-card` props](https://vuetifyjs.com/en/components/cards/) that will be passed to the card wrapping the the tabs. |
| **outer-tabs-[key]** | All [`v-tabs` props](https://vuetifyjs.com/en/components/tabs/) that will be passed the outer tabs (parents level). |
| **inner-tabs-[key]** | All [`v-tabs` props](https://vuetifyjs.com/en/components/tabs/) that will be passed the inner tabs (children level). |

**Remark 1**: All other attributes will be passed as is to `VEditor` and `Editor`.
**Remark 2**: `Editor`'s `enableDataInput` prop is set to false

### Events

| Name | Description |
| ---- | ----------- |
| **backup** | Passes {<span class="primary--text">data:</span> <span class="green--text">string</span>, <span class="primary--text">name:</span> <span class="green--text">string</span>} |


### Slots

`VSimpleCms` does not create any new slots, but all `VEditor` slots binds the following data:

| Name | type | Description |
| ---- | ---- | ----------- |
| **value** | Object | The data object |
| **id** | String| Current child key |
| **tab** | Number | Current child index |
| **key** | String | Current group key |
| **keyIndex** | Number | Current group index

### References

| Name | Descrition |
| ---- | ---------- |
| **cms-[key]** | Reference to the `CmsItem` component, which wrapps `VEditor`. **key** is a parent level key. |

### Useful Functions

```js
editorAccess (key: string, id: string) {
  return this.$refs[`cms-${key}`][0].$refs[`vscms-${id}`][0].$refs.editor
}
```

By providing a parent-level-key and an item's id we can access the `Editor` reference and its [useful functions](/#usefulfunction-2)

## `GoToFab`

A useful utility component is `GoToFab` which is a FAB button that receives a scroll function, x-position of the button and an icon and can programmatically scroll to an id, class or HTMLSelector.
By default two of these buttons are set in `Editor`. The up button is locked at the top of the editor and the down button to the v-card-actions part of the editor's card. They can be disabled by passing `disableGoToButton` as false to `VEditor` or `VSimpleCms`.
It might be useful for some users, which might have a couple of instances of `VEditor` or `VSimpleCms` in their web page, to disable the default buttons and import `GoToFab` in the default layout component (when using Nuxt.js for example). Such that there is only one instance of `GoToFab` that works exactly the same on every page of the web page.

::: details Show Code
```vue
<template>
  <v-app id="top">
    ..
    <v-footer id="bottom">..</v-footer>
    <div>
      <GoToFab 
        goTo="#top" 
        icon="mdi-chevron-up"
        direction="up"
      />
      <GoToFab 
        goTo="#bottom" 
        color="pink" 
        icon="mdi-chevron-down"
        direction="down"
      />
    </div>
  </v-app>
</template>

<script>
import GoToFab from "@/components/GoToFab.vue"
export default {
  components: {
    GoToFab
  },
  data: () => ({
    pos: 50
  }),
  upScrollY (v) {
    if (v < 50) {
      this.pos = 0
    } else {
      this.pos = 50
    }
    return (v >= 50)
  },
  downScrollY (v) {
    const height = document.documentElement.scrollHeight - document.documentElement.clientHeight
    return (v <= height - 50)
  }
}
</script>
```
:::

### Props

| Name | type | Default | Description |
| ---- | ---- | ------- | ----------- |
| **goTo** | <span class="green--text">[HTMLElement, String]</span> | **required** | An id, class or HTML selector to go to |
| **color** | <span class="green--text">String</span> | <span class="primary--text">"primary"</span> | Button color |
| **icon** | <span class="green--text">String</span> | **required** | Button icon |
| **direction** | String | <span class="primary--text">50</span> | scroll direction. Must be passed as "up" or "down" |

<br><br>

# Configuration

Every legal configuration requires the following structure:

1. **template** - An Object of all the necessary keys with their default value
2. **config** - An Object of all necessary keys and their corrosponding configuration:
  - **component** (String) - component name. Must comply with `legalComponentNames` list otherwise it will default to `v-text-field`
  - **props** (Object) - Can take any prop that is defined in the [vuetify documentation](https://vuetifyjs.com/en/api/vuetify/)
  - **parent** (Object) - As props and component attributes are overwritten / changed by the internal logic of `Editor` it is sometime's necessary to pass an extra configuration to the entire group. For example, hide a group which the end user does not need to edit/see like creation time or specific ids.
3. **searchKey** (String) - An optional setting of `VSimpleCms`. The key passed must exist in template and will be used for search. If not passed it will default to the key of the Object.

```json
{
  "template": {
    "draft": false,
    "internal": false,
    "name": "template",
    "short": "",
    "text": "",
    "index": ""
  },
  "config": {
    "draft": {
      "component": "v-checkbox"
    },
    "internal": {
      "component": "v-checkbox"
    },
    "name": {
      "component": "v-text-field"
    },
    "short": {
      "component": "v-textarea"
    },
    "createdBy": {
      "parent": {
        "hidden": true
      }
    },
    "createdAt": {
      "parent": {
        "hidden": true
      }
    },
    "updatedAt": {
      "parent": {
        "hidden": true
      }
    },
    "index": {
      "parent": {
        "hidden": true
      }
    }
  },
  "searchKey": "name"
}
```

And configuration of sub-items:

```json
{
  "template": {
    "items": [
      {
        "name": "",
        "url": ""
      }
    ]
  },
  "config": {
    "items.0.name": {
      "component": "v-text-field",
      "props": {
        "rules": ["required"],
        "label": "Name"
      }
    },
    "items.0.url": {
      "component": "v-text-field",
      "props": {
        "rules": ["email"],
        "label": "Email"
      }
    },
    "index": {
      "parent": {
        "hidden": true
      }
    }
  },
  "searchKey": "key"
}
```

# Types

### TemplateTypes

```ts
export interface TemplateTypes {
  value: string,
  text: string
}
```

### KeyRules

```ts
export interface RulesFunction {
  (value: string): string|boolean
}
export interface CustomObject<T> {
  [key: string]: T
}
export type KeyRules = CustomObject<RulesFunction>
```

